# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Marek Laane <qiilaq69@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-19 01:34+0000\n"
"PO-Revision-Date: 2019-11-08 22:20+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.1\n"

#: contents/ui/ConfigOverlay.qml:277 contents/ui/ConfigOverlay.qml:311
#, kde-format
msgid "Remove"
msgstr "Eemalda"

#: contents/ui/ConfigOverlay.qml:287
#, fuzzy, kde-format
#| msgid "Configure..."
msgid "Configure…"
msgstr "Seadista ..."

#: contents/ui/ConfigOverlay.qml:298
#, fuzzy, kde-format
#| msgid "Show Alternatives..."
msgid "Show Alternatives…"
msgstr "Näita alternatiive ..."

#: contents/ui/ConfigOverlay.qml:321
#, kde-format
msgid "Spacer width"
msgstr ""

#: contents/ui/main.qml:388
#, kde-format
msgid "Add Widgets…"
msgstr ""
