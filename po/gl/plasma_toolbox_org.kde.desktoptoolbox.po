# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-21 01:35+0000\n"
"PO-Revision-Date: 2023-06-18 16:54+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: contents/ui/ToolBoxContent.qml:267
#, kde-format
msgid "Choose Global Theme…"
msgstr "Escoller o tema global…"

#: contents/ui/ToolBoxContent.qml:274
#, kde-format
msgid "Configure Display Settings…"
msgstr "Configurar as pantallas…"

#: contents/ui/ToolBoxContent.qml:295
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Máis"

#: contents/ui/ToolBoxContent.qml:309
#, kde-format
msgid "Exit Edit Mode"
msgstr "Desactivar o modo de edición"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Rematar de personalizar a disposición"

#~ msgid "Default"
#~ msgstr "Predeterminado"

#~ msgid "Desktop Toolbox"
#~ msgstr "Caixa de ferramentas de escritorio"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "Caixa de ferramentas de escritorio — Actividade %1"

#~ msgid "Lock Screen"
#~ msgstr "Bloquear a pantalla"

#~ msgid "Leave"
#~ msgstr "Saír"
