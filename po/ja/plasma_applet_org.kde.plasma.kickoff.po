# Fuminobu TAKEYAMA <ftake@geeko.jp>, 2015.
# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2015, 2021.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_kickoff\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-25 01:35+0000\n"
"PO-Revision-Date: 2023-02-04 01:43+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.12.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "全般"

#: package/contents/ui/code/tools.js:32
#, kde-format
msgid "Remove from Favorites"
msgstr "お気に入りから削除"

#: package/contents/ui/code/tools.js:36
#, kde-format
msgid "Add to Favorites"
msgstr "お気に入りに追加"

#: package/contents/ui/code/tools.js:60
#, kde-format
msgid "On All Activities"
msgstr "すべてのアクティビティ"

#: package/contents/ui/code/tools.js:110
#, kde-format
msgid "On the Current Activity"
msgstr "現在のアクティビティ"

#: package/contents/ui/code/tools.js:124
#, kde-format
msgid "Show in Favorites"
msgstr "お気に入りに表示"

#: package/contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Icon:"
msgstr "アイコン:"

#: package/contents/ui/ConfigGeneral.qml:47
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr "アプリケーションランチャーのアイコンを変更"

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""
"現在のアイコンは %1 です。現在のアイコンを変更、もしくはデフォルトのアイコン"
"にリセットするには、クリックしてメニューを開いてください。"

#: package/contents/ui/ConfigGeneral.qml:52
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr "アイコンの名前は \"%1\" です。"

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "選択..."

#: package/contents/ui/ConfigGeneral.qml:87
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr "アプリケーションランチャーのアイコンを選択"

#: package/contents/ui/ConfigGeneral.qml:91
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "標準のアイコンに戻す"

#: package/contents/ui/ConfigGeneral.qml:97
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr "アイコンを削除"

#: package/contents/ui/ConfigGeneral.qml:108
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "テキストラベル:"

#: package/contents/ui/ConfigGeneral.qml:110
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "ここにタイプしてテキストラベルを追加"

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr "メニューラベルをリセット"

#: package/contents/ui/ConfigGeneral.qml:139
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr "パネルが垂直なときはテキストラベルを設定できません。"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "一般:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "常にアルファベット順でソート"

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgid "Use compact list item style"
msgstr "コンパクトなリストを使用する"

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr "タッチモード時自動的に無効化する"

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "検索プラグインを設定..."

#: package/contents/ui/ConfigGeneral.qml:181
#, kde-format
msgid "Sidebar position:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Right"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Left"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:198
#, kde-format
msgid "Show favorites:"
msgstr "お気に入り:"

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "グリッドで表示する"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "リストで表示する"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Show other applications:"
msgstr "他のアプリケーション:"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "グリッドで表示する"

#: package/contents/ui/ConfigGeneral.qml:224
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "リストで表示する"

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Show buttons for:"
msgstr "ボタンを表示:"

#: package/contents/ui/ConfigGeneral.qml:237
#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Power"
msgstr "電源"

#: package/contents/ui/ConfigGeneral.qml:246
#, kde-format
msgid "Session"
msgstr "セッション"

#: package/contents/ui/ConfigGeneral.qml:255
#, kde-format
msgid "Power and session"
msgstr "電源とセッション"

#: package/contents/ui/ConfigGeneral.qml:264
#, kde-format
msgid "Show action button captions"
msgstr "アクションボタンにキャプションを表示"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "アプリケーション"

#: package/contents/ui/Footer.qml:113
#, kde-format
msgid "Places"
msgstr "場所"

#: package/contents/ui/FullRepresentation.qml:153
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr "一致なし"

#: package/contents/ui/Header.qml:80
#, kde-format
msgid "Open user settings"
msgstr "ユーザ設定を開く"

#: package/contents/ui/Header.qml:257
#, kde-format
msgid "Keep Open"
msgstr "開けたままにする"

#: package/contents/ui/KickoffGridView.qml:92
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "%1行、%2列のグリッド"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Leave"
msgstr "終了"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "More"
msgstr "その他"

#: package/contents/ui/main.qml:316
#, kde-format
msgid "Edit Applications…"
msgstr "アプリケーションを編集..."

#: package/contents/ui/PlacesPage.qml:48
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "コンピュータ"

#: package/contents/ui/PlacesPage.qml:49
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "履歴"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "最近使用したもの"
